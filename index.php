<!DOCTYPE html>
<html lang="FR">
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <title>POP-ABS</title>
  <meta name="generator" content="Bootply" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!--[if lt IE 9]>
  <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link href="css/styles.css" rel="stylesheet">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery-2.0.3.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/site.js"></script>
</head>
<body>
  <!-- Header -->
  <div class="banner"> </div>
  <div id="top-nav" class="navbar navbar-inverse navbar-static-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-toggle"></span>
        </button>
        <a class="navbar-brand" href="index.php"><i class="glyphicon glyphicon-home"></i> POP ABSENCES</a>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">

          <li class="dropdown">
            <a href="index.php">
              <i class="glyphicon glyphicon-home"></i> Accueil</a></li>
              <li class="">
                <a href="cuser.php">
                  <i class="glyphicon glyphicon-user"></i> Etudiant</a></li>
                  <li class="">
                    <a href="cadmin.php">
                      <i class="glyphicon glyphicon-user"></i> Admin</a></li>

                    </ul>
                  </li>
                </ul>
              </div>
            </div><!-- /container -->
          </div>
          <!-- /Header -->

          <!-- Main -->
          <div class="container">

            <!-- upper section -->

            <div class="col-sm-12 page">

              <!-- column 2 -->
              <h3 class="text-center"><i class="glyphicon "></i> Bienvenue Sur Pop Absences</h3> <hr>


              <div class="row visible-md visible-lg">
                <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
                  <div class="carousel slide" id="theCarousel" data-interval="2000">

                    <ol class="carousel-indicators">
                      <li data-target="#theCarousel" data-slide-to="0" class="active"></li>
                      <li data-target="#theCarousel" data-slide-to="1"></li>
                    </ol>

                    <div class="carousel-inner">
                      <div class="item active">
                        <img src="images/abs1.jpg" alt="1" class="img-responsive" />
                        <div class="carousel-caption">
                          <h4>POP-ABSENCES</h4>
                          <p>Pour prévenir de vos absences</p>
                        </div>
                      </div>
                      <div class="item">
                        <img src="images/abs2.jpg" alt="2" class="img-responsive" />
                        <div class="carousel-caption">
                          <h4>POP-ABSENCES</h4>
                          <p>Et les justifier ! </p>
                        </div>
                      </div>


                    </div>

                    <a href="#theCarousel" class="carousel-control left" data-slide="prev"><span class="icon-prev"></span></a>
                    <a href="#theCarousel" class="carousel-control right" data-slide="next"><span class="icon-next"></span></a>
                  </div>
                </div>
              </div>

              <hr/>

            </body>
            </html>
            <?php
            ?>
