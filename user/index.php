<?php
session_start();

if(isset($_SESSION['user']) && isset($_SESSION['password'])){
  include "../config/mysql.php";
  ?>
  <!DOCTYPE html>
  <html lang="FR">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title><?php echo $_SESSION['user'];?></title>
    <meta name="generator" content="Bootply" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/sb-admin.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href="css/styles.css" rel="stylesheet">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

  </head>
  <body>

    <!-- Header -->
    <div id="top-nav" class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-toggle"></span>
          </button>
          <a class="navbar-brand" href="index.php"><i class="glyphicon glyphicon-user"></i> <?php echo $_SESSION['user'];?></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">

            <li class="dropdown">
              <a href="deconnexion.php">
                <i class="glyphicon glyphicon-log-out"></i> Déconnexion</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div><!-- /container -->
    </div>
    <!-- /Header -->

    <!-- Main -->
    <div class="container">

      <!-- upper section -->
      <div class="row">
        <div class="col-md-3">
          <!-- left -->
          <h3><i class="glyphicon glyphicon-list"></i> Menu</h3>
          <hr>

          <ul class="nav nav-stacked">

            <li><a href="index.php"><i class="glyphicon glyphicon-envelope"></i> Méssages </a></li>
            <li><a href="deconnexion.php"><i class="glyphicon glyphicon-log-out"></i> Déconnexion</a></li>
          </ul>



        </div><!-- /span-3 -->
        <div class="col-sm-9">

          <!-- column 2 -->
          <h3><i class="glyphicon glyphicon-envelope"></i> Envoi de Messages</h3>  <hr>
          <form method="post" action="index.php" enctype="multipart/form-data">
            <div class="form-group">
              <label class="">Date de l'absence :</label>
              <input type="date" name="dt" class="form-control" placeholder="Bach" required/>
            </div>
            <div class="form-group">
              <label class="">Message :</label>
              <Textarea name="msg" class="form-control" >
              </Textarea>

            </div>

            <div class="form-group">
              <label>Justificatif :</label>
              <input type="file"name="files" accept=".pdf, .csv, .jpg, .png"  class="form-control" required/>
            </div>
            <div class="form-group">
              <input type="submit"class="btn btn-success" id="conbt"class="form-control" value="ENVOYER" style="width: 100%;"/>
            </div></form>
            <?php
            if(isset($_POST['dt'])and isset($_POST['msg'])and isset($_FILES['files']['name'])) {
              $file = $_FILES['files']['name'];
              move_uploaded_file($_FILES['files']['tmp_name'], '../upload/' .$file);
              $user = $_SESSION['user'];
              $dt = $_POST['dt'];
              $msg = $_POST['msg'];
              mysql_query("INSERT INTO message VALUES(null,'$user','$file','$msg', '$dt')");
              header("location:index.php");
            }

            ?>
          </div>
        </div><hr>
      </body>
      </html>

      <?php
    }else header("location:../cadmin.php");

    ?>
