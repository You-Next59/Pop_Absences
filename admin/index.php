<?php
session_start();

if(isset($_SESSION['user']) && isset($_SESSION['password'])){
  include "../config/mysql.php";
  ?>
  <!DOCTYPE html>
  <html lang="FR">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>ADMIN-PANEL</title>
    <meta name="generator" content="Bootply" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/sb-admin.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href="css/styles.css" rel="stylesheet">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>

    <!-- Header -->
    <div id="top-nav" class="navbar navbar-inverse navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-toggle"></span>
          </button>
          <a class="navbar-brand" href="index.php"><i class="glyphicon glyphicon-home"></i> ADMIN-PANEL</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">

            <li class="dropdown">
              <a href="deconnexion.php">
                <i class="glyphicon glyphicon-log-out"></i> Déconnexion</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div><!-- /container -->
    </div>
    <!-- /Header -->

    <!-- Main -->
    <div class="container">

      <!-- upper section -->
      <div class="row">
        <div class="col-md-3">
          <!-- left -->
          <h3><i class="glyphicon glyphicon-list"></i> Menu</h3>
          <hr>

          <ul class="nav nav-stacked">
            <li class="active"><a href="index.php"><i class="glyphicon glyphicon-home"></i> Accueil</a></li>
            <li><a href="user.php"><i class="glyphicon glyphicon-user"></i> Listes des étudiants</a></li>
            <li><a href="promotion.php"><i class="glyphicon glyphicon-list-alt"></i> Listes des promotions</a></li>
            <li><a href="absence.php"><i class="glyphicon glyphicon-time"></i> Listes des absences</a></li>
            <li><a href="stats.php"><i class="glyphicon glyphicon-stats"></i> Statistiques</a></li>
            <li><a href="administrateur.php"><i class="glyphicon glyphicon-dashboard"></i> Ajouter un administrateur </a></li>
            <li><a href="message.php"><i class="glyphicon glyphicon-envelope"></i> Méssages </a></li>
            <li><a href="deconnexion.php"><i class="glyphicon glyphicon-log-out"></i> Déconnexion</a></li>
          </ul>



        </div><!-- /span-3 -->
        <div class="col-sm-9">

          <!-- column 2 -->
          <h3><i class="glyphicon glyphicon-dashboard"></i> Tableau de bord</h3>  <hr>
          <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    <i class="fa fa-comments fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                    <div class="huge"><?php// calcule("etudiant");?></div>
                    <div>Etudiants</div>
                  </div>
                </div>
              </div>
              <a href="user.php">
                <div class="panel-footer">
                  <span class="pull-left">Afficher les détails</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    <i class="fa fa-tasks fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                    <div class="huge"><?php// calcule("enseignant");?></div>
                    <div>Promotions</div>
                  </div>
                </div>
              </div>
              <a href="promotion.php">
                <div class="panel-footer">
                  <span class="pull-left">Afficher les détails</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    <i class="fa fa-shopping-cart fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                    <div class="huge"><?php //calcule("cours");?></div>
                    <div>Absences</div>
                  </div>
                </div>
              </div>
              <a href="absence.php">
                <div class="panel-footer">
                  <span class="pull-left">Afficher les détails</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    <i class="fa fa-support fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                    <div class="huge"><?php //calcule("module");?></div>
                    <div>Méssages</div>
                  </div>
                </div>
              </div>
              <a href="message.php">
                <div class="panel-footer">
                  <span class="pull-left">Afficher les détails</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div><hr>

    </body>
    </html>
    <?php
  }else header("location:../cadmin.php");

  ?>
